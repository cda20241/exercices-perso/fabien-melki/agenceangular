import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegisterComponent } from './pages/register/register.component';
import { LoginComponent } from './pages/login/login.component';
import { HeaderComponent } from './pages/header/header.component';
import { MainComponent } from './pages/main/main.component';
import { AllbiensComponent } from './pages/allbiens/allbiens.component';
import { AllusersComponent } from './pages/allusers/allusers.component';
import { BiendetailsComponent } from './pages/biendetails/biendetails.component';
import { UserdetailsComponent } from './pages/userdetails/userdetails.component';
import { AdduserComponent } from './pages/adduser/adduser.component';
import { AddbienComponent } from './pages/addbien/addbien.component';
import { NotfoundComponent } from './pages/notfound/notfound.component';
import { HttpClientModule } from '@angular/common/http';
import { AccueilComponent } from './pages/accueil/accueil.component';
import { ReactiveFormsModule } from '@angular/forms';
import { TokenInterceptorProvider } from './services/token.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    HeaderComponent,
    MainComponent,
    AllbiensComponent,
    AllusersComponent,
    BiendetailsComponent,
    UserdetailsComponent,
    AdduserComponent,
    AddbienComponent,
    NotfoundComponent,
    AccueilComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule, 
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [TokenInterceptorProvider],
  bootstrap: [AppComponent]
})
export class AppModule { }
