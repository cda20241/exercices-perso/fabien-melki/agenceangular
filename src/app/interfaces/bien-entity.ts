import { PersonneEntity } from "./personne-entity";

export interface BienEntity {
    id: number,
    rue : string,
    ville : string,
    codePostal : number,
    type : string,
    personneBien : PersonneEntity[];

 



}
