export interface PersonneEntity {
    id?: number,
    nom: string,
    prenom: string,
    rue : string,
    ville : string,
    codePostal : string,
    email: string,
    password: string,
    role: string
}
