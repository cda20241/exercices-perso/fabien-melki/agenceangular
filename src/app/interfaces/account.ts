export interface Account {
    token : string,
    role : string,
}

export class AccountImpl {
    token : string;
    role : string;
constructor(account?: Account) {
    this.role=account?.role || "";
    this.token=account?.token || "";
}}
