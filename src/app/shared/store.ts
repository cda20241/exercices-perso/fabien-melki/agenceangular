import { Account } from "../interfaces/account";

export class MyStore {
    
  private account?: Account | null;
  constructor() {}

  setAccount(object: Account) {
    this.account = object;
  }

  getAccount() {
    return this.account;
  }

  destroy(){
    this.account=null;
  }
}