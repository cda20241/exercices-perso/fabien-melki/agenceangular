import { Injectable } from '@angular/core';
import { BienEntity } from '../interfaces/bien-entity';
import { Observable } from 'rxjs';
import { environement } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BienService {


  baseUrl = environement.apiUrl + '/bien';

  constructor(
    private http: HttpClient
  ) { }

  createBien(bien: BienEntity): Observable<BienEntity> {
    return this.http.post<BienEntity>(this.baseUrl + '/ajouter', bien);
  }

  getBiens(): Observable<BienEntity[]> {
    return this.http.get<BienEntity[]>(this.baseUrl + '/all');
  }

  getBien(id: number): Observable<BienEntity> {
    return this.http.get<BienEntity>(this.baseUrl + '/' + id);
  }

  modifyBien(bien: BienEntity): Observable<BienEntity> {
    return this.http.put<BienEntity>(this.baseUrl + '/modifier/' + bien.id, bien);
  }

  deleteBien(id: number): Observable<BienEntity> {
    return this.http.delete<BienEntity>(this.baseUrl + '/supprimer/' + id);
  }

  filtrerBien(type: string): Observable<BienEntity[]> {
    return this.http.get<BienEntity[]>(this.baseUrl + '/filtre/' + type);
  }
}
