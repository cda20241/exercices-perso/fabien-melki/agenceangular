import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environement } from 'src/environments/environment';
import { Account, AccountImpl } from '../interfaces/account';
import { Observable, lastValueFrom } from 'rxjs';
import { Login } from '../interfaces/login';
import { Register } from '../interfaces/register';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  baseUrl = environement.apiUrl + '/auth';
  account = new AccountImpl();
  
  constructor(private http: HttpClient, private router: Router) {}

  login(account: Login): Observable<Account>{
    return this.http.post<Account>(this.baseUrl + '/authenticate', account)
  }

  createAccount(account:Account){
    this.account = account;
  }

  register(register:Register): Observable<Register>{
    return this.http.post<Register>(this.baseUrl + '/authenticate', register)
  }

  isLogged(){
    const token = this.account.token
    return !! token
  }
  
  getAccountRole(){
    return this.account.role
  }

  getToken(){
    return this.account.token
  }
  
  logout(){
    this.account=new AccountImpl();
    this.router.navigate(['/login'])
  }


}
