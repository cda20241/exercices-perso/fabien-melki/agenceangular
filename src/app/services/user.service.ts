import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environement } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  baseUrl = environement.apiUrl + '/personne';

  constructor(
    private http: HttpClient
  ) { }

  createUser(user: any): Observable<any> {
    return this.http.post(this.baseUrl + '/ajouter', user);
  }

  getUsers(): Observable<any> {
    return this.http.get(this.baseUrl + '/all');
  }

  getUser(id: number): Observable<any> {
    return this.http.get(this.baseUrl + '/' + id);
  }

  modifyUser(user: any): Observable<any> {
    return this.http.put(this.baseUrl + '/modifier/' + user.id, user);
  }

  deleteUser(id: number): Observable<any> {
    return this.http.delete(this.baseUrl + '/supprimer/' + id);
  }

  filtrerUser(role: string): Observable<any> {
    return this.http.get(this.baseUrl + '/filtre/' + role);
  }

  getBienbyUser(id: number): Observable<any> {
    return this.http.get(this.baseUrl + '/info_biens_de_la_personne/' + id);
  }

  addBienToUser(bienId: number, userId: number): Observable<any> {
    return this.http.get(this.baseUrl + '/ajouter_bien_a_la_personne/' + userId + '/' + bienId);
  }

  deleteBienToUser(bienId: number, userId: number): Observable<any> {
    return this.http.get(this.baseUrl + '/supprimer_bien_a_la_personne/' + userId + '/' + bienId);
  }
}
