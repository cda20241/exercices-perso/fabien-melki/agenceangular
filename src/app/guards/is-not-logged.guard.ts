import { Injectable } from '@angular/core';
import { CanActivate, CanActivateFn, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root',
})
export class IsNotLoggedGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}
  canActivate(){
    return this.checkUserLogin();
  }
  checkUserLogin(): boolean{
    if(this.authService.isLogged()){
      this.router.navigate(["/accueil"])
      return false
    }
    return true
  }
}
