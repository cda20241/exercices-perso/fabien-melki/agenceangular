import { Injectable } from '@angular/core';
import { CanActivate, CanActivateFn, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { AccountImpl } from '../interfaces/account';

@Injectable({
  providedIn: 'root',
})
export class IsLoggedGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}
  canActivate(){
    return this.checkUserLogin();
  }
  checkUserLogin(): boolean{
    if(this.authService.isLogged()){
      return true
    }
    this.router.navigate(['/login'])
    return false
  }

}


