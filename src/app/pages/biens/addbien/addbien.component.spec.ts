import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddbienComponent } from './addbien.component';

describe('AddbienComponent', () => {
  let component: AddbienComponent;
  let fixture: ComponentFixture<AddbienComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AddbienComponent]
    });
    fixture = TestBed.createComponent(AddbienComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
