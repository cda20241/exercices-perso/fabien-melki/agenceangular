import { Component, EnvironmentInjector, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BienService } from 'src/app/services/bien.service';
import { BienEntity } from 'src/app/interfaces/bien-entity';
import { AuthService } from 'src/app/services/auth.service';
import { environement } from 'src/environments/environment';


@Component({
  selector: 'app-allbiens',
  templateUrl: './allbiens.component.html',
  styleUrls: ['./allbiens.component.scss']
})
export class AllbiensComponent implements OnInit{
 
  biens: BienEntity[] | null = null; 

  constructor(
    private http: HttpClient, 
    private bienService: BienService,
    private authService: AuthService
  ) { }

 ngOnInit(): void {

  this.bienService.getBiens().subscribe((biens) => {
    this.biens = biens;
  });

 }
 
}
