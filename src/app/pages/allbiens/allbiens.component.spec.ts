import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllbiensComponent } from './allbiens.component';

describe('AllbiensComponent', () => {
  let component: AllbiensComponent;
  let fixture: ComponentFixture<AllbiensComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AllbiensComponent]
    });
    fixture = TestBed.createComponent(AllbiensComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
