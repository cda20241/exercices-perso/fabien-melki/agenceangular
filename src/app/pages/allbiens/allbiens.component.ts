import { Component, Injectable, OnInit } from '@angular/core';
import { BienService } from 'src/app/services/bien.service';
import { BienEntity } from 'src/app/interfaces/bien-entity';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';



@Component({
  selector: 'app-allbiens',
  templateUrl: './allbiens.component.html',
  styleUrls: ['./allbiens.component.scss']
})
export class AllbiensComponent implements OnInit{

 biens!:BienEntity[];

 constructor(
      private bienService: BienService,
      private router: Router, private authService: AuthService)
      { }

  ngOnInit(): void {
    // this.bienService.getBiens().subscribe(
    //   (biens) => console.log(biens) )
    
  }

}
