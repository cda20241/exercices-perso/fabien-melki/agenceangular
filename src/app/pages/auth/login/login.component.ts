import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Account, AccountImpl } from 'src/app/interfaces/account';
import { Login, LoginImpl } from 'src/app/interfaces/login';
import { AuthService } from 'src/app/services/auth.service';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  formLogin : FormGroup = new FormGroup({
    email : new FormControl("", Validators.required), 
    password : new FormControl("", Validators.required),
  })
  constructor(private router : Router,private authService : AuthService){}
  async login(){
    if (this.formLogin.valid){
      let login : LoginImpl = new LoginImpl();
      login.email=this.formLogin.get("email")!.value as string; 
      login.password=this.formLogin.get("password")!.value as string;
      const data = await this.authService.login(login);
      if (data) {
        this.authService.createAccount(data);
        this.router.navigate(["/accueil"])
    }
    }
  }

}
