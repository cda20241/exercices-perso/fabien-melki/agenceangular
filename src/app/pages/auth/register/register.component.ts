import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PersonneEntity } from 'src/app/interfaces/personne-entity';
import { Register } from 'src/app/interfaces/register';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {
  formulaire : FormGroup = new FormGroup({
                              nom : new FormControl("", Validators.required),
                              prenom : new FormControl("", Validators.required), 
                              email : new FormControl("", Validators.required), 
                              rue : new FormControl("", Validators.required), 
                              ville : new FormControl("", Validators.required), 
                              codePostal : new FormControl("", Validators.required), 
                              password : new FormControl("", Validators.required),
                              role : new FormControl("", Validators.required)
                            })
  constructor(private authService : AuthService,
    private router: Router) {}

  async createUser(){
    let register = {} as PersonneEntity;
    register.nom=this.formulaire.get("nom")!.value as string;
    register.prenom=this.formulaire.get("prenom")!.value as string;
    register.rue=this.formulaire.get("rue")!.value as string;
    register.ville=this.formulaire.get("ville")!.value as string;
    register.codePostal=this.formulaire.get("codePostal")!.value as string;
    register.email=this.formulaire.get("email")!.value as string;
    register.password=this.formulaire.get("password")!.value as string;
    register.role=this.formulaire.get("role")!.value as string;
    console.log(this.formulaire)
    const data = await this.authService.register(register);
    if (data) {
        this.router.navigate(["/login"])
    }
    
  }



}
