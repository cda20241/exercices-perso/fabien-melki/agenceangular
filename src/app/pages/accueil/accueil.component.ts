import { Component, OnInit } from '@angular/core';
import { BienEntity } from 'src/app/interfaces/bien-entity';
import { PersonneEntity } from 'src/app/interfaces/personne-entity';
import { BienService } from 'src/app/services/bien.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.scss']
})
export class AccueilComponent implements OnInit {
  users?:PersonneEntity[];
  constructor(private bienService:BienService, private userService:UserService){}
  ngOnInit() {
    this.userService.getUsers().subscribe(
      (users:PersonneEntity[]) => {
        this.users=users;
      }
    )
    
  }
}
