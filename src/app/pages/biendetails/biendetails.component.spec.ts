import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BiendetailsComponent } from './biendetails.component';

describe('BiendetailsComponent', () => {
  let component: BiendetailsComponent;
  let fixture: ComponentFixture<BiendetailsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BiendetailsComponent]
    });
    fixture = TestBed.createComponent(BiendetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
