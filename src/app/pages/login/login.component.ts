import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Account, AccountImpl } from 'src/app/interfaces/account';
import { Login } from 'src/app/interfaces/login';
import { AuthService } from 'src/app/services/auth.service';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  formLogin : FormGroup = new FormGroup({
    email : new FormControl("", Validators.required), 
    password : new FormControl("", Validators.required),
  })
  constructor(private router : Router,private authService : AuthService){}
  async login(){
    if (this.formLogin.valid){
      let login={} as Login;
      login.email=this.formLogin.get("email")!.value as string; 
      login.password=this.formLogin.get("password")!.value as string;
      this.authService.login(login).subscribe(
        (account: Account) => {
          this.authService.createAccount(account);
          this.router.navigate(["/accueil"])
        },
      );
    }
  }

}
