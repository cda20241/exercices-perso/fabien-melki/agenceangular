import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { MainComponent } from './pages/main/main.component';
import { AllbiensComponent } from './pages/allbiens/allbiens.component';
import { AllusersComponent } from './pages/allusers/allusers.component';
import { BiendetailsComponent } from './pages/biendetails/biendetails.component';
import { UserdetailsComponent } from './pages/userdetails/userdetails.component';
import { AdduserComponent } from './pages/adduser/adduser.component';
import { AddbienComponent } from './pages/addbien/addbien.component';
import { NotfoundComponent } from './pages/notfound/notfound.component';
import { AccueilComponent } from './pages/accueil/accueil.component';
import { IsNotLoggedGuard } from './guards/is-not-logged.guard';
import { IsLoggedGuard } from './guards/is-logged.guard';


const routes: Routes = [
  { path: '', redirectTo: 'accueil', pathMatch: 'full'},
  { path: 'login', component: LoginComponent, canActivate:[IsNotLoggedGuard]},
  { path: 'register', component: RegisterComponent, canActivate:[IsNotLoggedGuard]},
  { path: '' , component: MainComponent, canActivate:[IsLoggedGuard], children: [
                  {path: 'accueil', component: AccueilComponent},
                  {path: 'allbiens', component: AllbiensComponent},
                  {path: 'allusers', component: AllusersComponent},
                  {path: 'biendetails/:id', component: BiendetailsComponent},
                  {path: 'userdetails/:id', component: UserdetailsComponent},
                  {path: 'adduser', component: AdduserComponent},
                  {path: 'addbien', component: AddbienComponent}
  ]
  },
  {path: 'notfound', component: NotfoundComponent},
  {path : '**', redirectTo: 'notfound', pathMatch: 'full'}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
